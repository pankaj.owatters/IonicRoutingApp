           ------IONIC VS NATIVESCRIPT -----------

As, i've used both nativescript and ionic framework for making some small app.
so i find some of the differences beside the similarity(i.e. both are really good framework for building hybrid apps)

As in case of routing from one page to another i find ionic a little bit easy to use in comparison to nativescript as it automatically provide the back button by which
we can navigate to a page back very easily.

 but there are some pros and cons for both framework


------Pros of Ionic framework ----------
1.=> Ionic is easy to use as it have several of its own component like <ion-navbar>,
<ion-header>, <ion-item> etc
2.=> It works smoothly with angular
3.=> it works along with cordova that is based on html, css, js, do that is good for one who know these web languages

As after studying some article or by my own little experience, here are the cons of ionic:-
----Cons of ionic----
1.=> IOnic apps are run in a web view, not as an pure native apps.
2.=> Ionic has its own component, so it doesn't support some native of the native components
3.=> its layout don't use native component

---------Pros of Nativescript over ionic------
1.=>Complete native platform layouts
2=> App doesn't open in a web view
3=> newer and much popular

conclusion: Both the ionic and nativescript have their own pros and cons
            but what i find is that ionic is more easy to use but somewhere i know if it is 
about making native apps, then nativescript got the point as it support complete native component
for making a good native app.
